The project is divided into two applications. Both are developed on Python 2.7.9 and Flask as the framework.

The users app registers the user and the referee app controls the game.

requirements.txt provides the required libraries info. 

Also, the app is not bound to any hardcode depdendencies. It can even accomodate 4 members instead of the 
required 8 for a championship game(you need to configure the champion_size varaible in the settings file of the referee app).

The user app would shutdown if it is started before the game is intialized, as it cannot register.

To start the referee app, install and activate the virtual env.

the cd in referee directory and type,

python app.py

To start the users app, you can use the same virtual env.
cd into the users directory and type,

python app.py

On startup of the users app, the app would read all user info from the player_list.json file and
start registering them.

Upon finding the required quorum the referee app would intiate the game and publish a report on the completion of the same.
The published report can be found in the referee app directory. The file name is PingPong.txt