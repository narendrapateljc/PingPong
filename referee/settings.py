'''
	Global Settings file for the referee app.
'''
# pylint: disable=C0326

__CHAMPIONSHIP__SIZE__ 				= 		8
__USER__OFFENSIVE__ENDPOINT__		=		"http://127.0.0.1:5020/offensive/"
__USER__DEFENSIVE__ENDPOINT__		=		"http://127.0.0.1:5020/defensive/"
__MAX__SCORE__						=		5
__REPORT__FILE__NAME__				=		"PingPong"
__REPORT__FILE__EXTENSION__			=		"txt"
