
'''
    The PING PONG Championship Game
    Authour : Narendra Patel
'''
import json
import subprocess
from flask import Flask
from glom import glom
import settings

APP = Flask(__name__)

'''Keeping a Global data here for now. Ideally it sholud store the 
    data in a strcture like queue.
'''
__GLOBAL_DATA__ = {}
__GLOBAL_DATA__["WAITING_LIST"] = []


@APP.route('/register/<player_id>/<player_name>', methods=["GET"])
def register(player_id, player_name):
    '''
        Endpoint to register players for the championship.
        Here we queue partcipants and when the list equals 8(configurable via settings file),
        we start the game and remove them from the waiting queue.
    '''
    waiting_list = __GLOBAL_DATA__["WAITING_LIST"]
    championship_size = settings.__CHAMPIONSHIP__SIZE__

    # check if the user is already registered. If yes, do not re-register.
    if player_id not in glom(waiting_list, ['player_id']):

        player = {"player_id":player_id, "player_name":player_name}
        waiting_list.append(player)

    # removing players from waiting queue if quorum id formed and starting the game.
    if len(waiting_list) >= championship_size:

        player_list = waiting_list[:championship_size]

        subprocess.Popen(['python', 'start_championship.py', str(player_list)])
        __GLOBAL_DATA__["WAITING_LIST"] = waiting_list[championship_size:]
    
    # returning registration success message to the player.
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

if __name__ == '__main__':
    APP.run(host="0.0.0.0", port=5010, debug=True, threaded=True)
