'''
    Ping Pong Game
'''

def play(player_list, report_file):
    '''
        Starts a Game between players, keeps track of scores and
        returns the winner.
    '''
    import requests
    import ast
    import settings

    winner = None

    player_1 = player_list[0]
    player_2 = player_list[1]

    play_order = [player_1["player_id"], player_2["player_id"]]

    score_card = {player_1["player_id"]:{"score":0}, player_2["player_id"]:{"score":0}}

    report_file.write("\n\n\n****Game Begins****\n\n")
    report_file.write("Contenders : " + player_1["player_name"] +"(" + player_1["player_id"]+ ")\t")
    report_file.write(player_2["player_name"] +"(" + player_2["player_id"]+ ")\n")

    while True:

        ping = requests.get(settings.__USER__OFFENSIVE__ENDPOINT__+ str(play_order[0])).text
        pong = requests.get(settings.__USER__DEFENSIVE__ENDPOINT__+ str(play_order[1])).text

        report_file.write("\n\n\nOffensive is : " + str(play_order[0]))
        report_file.write("\nDefensive is : " + str(play_order[1]))

        pong = ast.literal_eval(pong)

        report_file.write("\nPing : " + ping)
        report_file.write("\nPong : " + str(pong))
        
        if int(ping) not in pong:
            score_card[play_order[0]]["score"] += 1
        else:
            score_card[play_order[1]]["score"] += 1
            play_order.reverse()

        if score_card[play_order[0]]["score"] == settings.__MAX__SCORE__:
            winner = play_order[0]
            break

        if score_card[play_order[1]]["score"] == settings.__MAX__SCORE__:
            winner = play_order[1]
            break
            
    for player in player_list:
        if player["player_id"] == winner:
            report_file.write("\n\nGame Winner is : " + winner)
            report_file.write("\n\n****Game Ends****\n\n")
            return player
            
