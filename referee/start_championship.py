'''
    Starting the Championship
'''
import sys
import ast
from random import shuffle
from glom import glom
from ping_pong import play
import settings

if __name__ == '__main__':

    # Controls the championship, co-ordinates each game,
    # track scores and generates championship summary report.


    # Read the participant players list from command line
    PLAYER_LIST = sys.argv[1:]
    PLAYER_LIST = ast.literal_eval(PLAYER_LIST[0])
    
    ROUND = 1

    # Acquiring file handle for summary report.
    REPORT_FILE = open(settings.__REPORT__FILE__NAME__ + "." +  \
                  settings.__REPORT__FILE__EXTENSION__, "w")


    # Writing to report fie.
    REPORT_FILE.write("\n\n#######################[Championship Begins]###############\n\n\n\n")



    while True:
        CURRENT_WINNERS = []

        # Shuffle players to avoid bias.
        shuffle(PLAYER_LIST)

        # Writing to report file
        REPORT_FILE.write("\n\n*************************************************************\n\n")
        REPORT_FILE.write("Round : " + str(ROUND))
        REPORT_FILE.write("\nInitial draw : \n")
        REPORT_FILE.write(str(glom(PLAYER_LIST, ['player_name'])))


        # Creating player pools for games. Each pool of size 2.
        __PLAYER_POOLS__ = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]
        PLAYER_POOLS = __PLAYER_POOLS__(PLAYER_LIST, 2)
        
        # Starting the game for each player pool.
        for PLAYER_POOL in PLAYER_POOLS:
            # Appending the game results to the intermediatery round storage.
            CURRENT_WINNERS.append(play(PLAYER_POOL, REPORT_FILE))

        # Wrting round winners information to file.
        REPORT_FILE.write("\nCurrent Round Winners :")
        REPORT_FILE.write(str(glom(CURRENT_WINNERS, ['player_name']))+"\n\n")


        # Check if all rounds have been completed and we have a complete winner.
        # If yes, then write it to the report and break the loop. Else continue.
        if len(CURRENT_WINNERS) == 1:
            
            # Writing Championship winner to file
            REPORT_FILE.write("\n\n\n\n#########################################################\n")
            REPORT_FILE.write("CHAMPIONSHIP WINNER IS : " + str(CURRENT_WINNERS[0]))
            REPORT_FILE.write("\n#########################################################\n")
            break

        # Assigning current round winners to be carry forward to next round.
        PLAYER_LIST = CURRENT_WINNERS

        # Incrementing round count.
        ROUND += 1

    # Writing end values and closing report file.
    REPORT_FILE.write("****Championship Ends****")
    REPORT_FILE.close()
