
'''
    The PING PONG Championship Game
    Authour : Narendra Patel
'''

# pylint: disable=W0703, W0613

import  json
from random import randint, sample
import logging
from logging.handlers import RotatingFileHandler
import requests
from flask import Flask
import settings


APP = Flask(__name__)

# Storing the player list in the gobal data for now. Ideally it sholud be
# stored in the DB or cached for optimized results

__GLOBAL_DATA__ = {}
__GLOBAL_DATA__["PLAYER_LIST"] = {}

@APP.route('/offensive/<user_id>')
def offensive(user_id):
    '''
        returns a random generated number for the called user id.
    '''
    return str(randint(1, 10))

@APP.route('/defensive/<user_id>')
def defensive(user_id):
    '''
        returns a defensive array for the called user id,
        according to thr defense set length.
    '''
    return str(sample(range(10), __GLOBAL_DATA__["PLAYER_LIST"][int(user_id)]["defence_set"]))
    

if __name__ == '__main__':

    #################[ Logging Configuration ]#########################

    LOG_HANDLER = RotatingFileHandler('info.log', maxBytes=100000, backupCount=1)
    LOG_HANDLER.setLevel(logging.INFO)
    APP.logger.setLevel(logging.INFO)
    APP.logger.addHandler(LOG_HANDLER)    

    ###################################################################


    #################[ Registering Players ]###############################
    with open(settings.__USER_FILE__) as f:
        DATA = json.load(f)

    def register_players(players_list):
        '''
            Registering players for the game.
        ''' 
        try:

            for player in players_list["players"]:

                __GLOBAL_DATA__["PLAYER_LIST"][player["player_id"]] = player

                APP.logger.info("registering player : %s", player)
                requests.get(settings.__GAME_REGISTER_URL__     \
                    + str(player["player_id"]) + "/" + player["player_name"])

        except Exception:
            # Exiting the App if the player registration fails
            APP.logger.exception("Failed to register user for Game")
            raise

    register_players(DATA)
    ###################################################################



    APP.run(host="0.0.0.0", port=5020)
